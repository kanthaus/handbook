# :wave: Introduction

Welcome to the [Kanthaus](https://kanthaus.online) handbook :smile:

It is the user guide for people who want to know how things work around the house.
It's always going to be a work-in-progress, as is Kanthaus, and is meant to:

- help residents operate the house :wrench:
- share our best-practices with interested parties elsewhere :outbox_tray:
- provide a space to store instructions when creating a new system :floppy_disk:
- give visitors-to-be as much insight as possible into what they're getting themselves into... :stuck_out_tongue_winking_eye:

### Chapters

Currently the Kanthaus handbook consists of the following chapters (which you can always access in the sidebar):

#### :information_desk_person: Practicalities

Guides and instructions for practical everyday needs.

#### :family: Social infrastructure

Customs and rules which structure life in Kanthaus.

#### :satellite: Technical infrastructure

All the more or less quirky technological solutions we have, from heating to house-bus.

#### :cloud: Digital infrastructure

Online spaces which belong to Kanthaus.

#### :house: Rooms

Individual rooms in Kanthaus, described in varying detail.


### Contribute

If you want to contribute to this handbook, head over to the [handbook](handbook.md) section :smile: