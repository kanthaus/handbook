# kanthaus.online

<img src="/images/underConstruction.jpg" alt="under construction yeah" style="width:150px;"/>

The website of Kanthaus.

## Contents and scope

[kanthaus.online](https://kanthaus.online) is a place to present our projects and Kanthaus itself. It's where people can find our [core documents](/social/documents.md), the list of current Volunteers and Members, our [meeting minutes](https://kanthaus.online/governance/minutes), the list of people's projects on the landing page and more.

### Website vs. handbook

The target audience is similar to this handbook:
- people who are just interested in Kanthaus
- people who want to visit Kanthaus

But in contrast to the handbook, which is about providing detailed guides and instructions, the website is much more about presenting and inviting.

