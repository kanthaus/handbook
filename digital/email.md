# :e-mail: Email accounts

We have email accounts hosted on [Uberspace.de](https://uberspace.de/en/).

## For users

Ask an existing volmem familiar with email administration to [create an account for you](#for-administrators).
We recommend using Thunderbird to access Kanthaus email, which will let you access Kanthaus email alongside your other email accounts, provide offline access to your emails and simplify the process of responding to Kanthaus emails.

### Accessing Kanthaus email on the web

If you don't want to use a client like Thunderbird, you can  access the email online at https://webmail.uberspace.de/.

When replying to hello@ messages, remember to add the `hello` address in both Reply-to: and Cc:.

It is also worth configuring your account to display a name instead of just your email address. In the top right corner, click on the menu -> Settings. Then click on "Identity: <your-name>@kanthaus.online" and set a name there.

### Setting up Kanthaus email in Thunderbird

Open up Thunderbird, go to "Tools -> Account settings", and create a new account with your Kanthaus email address and password just created by an administrator for you:

<img src="../images/thunderbird_tutorial/1_basic_conf.png" alt="Screenshot of initial configuration screen in Thunderbird" style="width: inherit" />

If Thunderbird fails to detect the settings automatically, you might have to adjust the settings as follows:
* Server (both for IMAP and SMTP): gehrels.uberspace.de
* Connection security: STARTTLS
* IMAP port: 143
* SMTP port: 587

<img src="../images/thunderbird_tutorial/2_detailed_conf.png" alt="Screenshot of server settings configuration screen in Thunderbird" style="width: inherit" />

Once your account has been created, go to the settings:

<img src="../images/thunderbird_tutorial/3_account_created.png" alt="Screenshot of the link to account settings after account creation" style="width: inherit" />

Click the "Manage Identities…" button and click "Add" to create a new identity:

<img src="../images/thunderbird_tutorial/4_manage_identities.png" alt="Screenshot of the dialog to manage identities" style="width: inherit" />

Use the following settings:
* Your name: the same name as you would use on your normal account
* Email address: still the same address
* Reply-to address: the `hello` address (@kanthaus.online).
* Organization: Kanthaus
* Identity Label: Hello@Kanthaus (or anything else you want - but it cannot be left empty)
* Tick "Reply from this identity when delivery headers match and add the `hello` address there as well. **Note**: this field is not immediately visible: you need to leave the dialog and edit the identity again for it to appear.

This will ensure that whenever you are replying to common email, you are telling your recipients to reply to the common address and not to you personally.

<img src="../images/thunderbird_tutorial/5_identity_configuration.png" alt="Screenshot of the identity settings" style="width: inherit" />

Then, go to the "Copies & Folders" tab and tick "Cc these email addresses" and add the `hello` address again. This will ensure that other Kanthausians are seeing your replies.

<img src="../images/thunderbird_tutorial/6_cc_configuration.png" alt="Screenshot of the copies and folders tab" style="width: inherit" />

That's it! You can close the dialog with "OK". You are now ready to reply to Kanthaus email. They are located in the "hello" folder under your account:

<img src="../images/thunderbird_tutorial/7_folders.png" alt="Screenshot of the folders in a typical Kanthaus email account" style="width: inherit" />

When replying to Kanthaus email we recommend you will automatically have the `hello` address in Cc: and Reply-To:, so that further replies are visible for others.
When that is not wished, you can remove those manually.

<img src="../images/thunderbird_tutorial/8_composing_email.png" alt="Screenshot of replying to an email with default settings" style="width: inherit" />

## For administrators

### Creating a new personal email account

Login to Uberspace's dashboard with Kanthaus' account and go to [the Email tab](https://dashboard.uberspace.de/dashboard/mail).
At the bottom of the page you will find "Virtual Mailboxes" section where you can add your own. Pick a fresh password for it.

### Giving access to Kanthaus email to a personal account

Once you have created a personal account, you can set it up so that it contains the general email received by Kanthaus (at the `hello` address)
as a subfolder. You can set it up as follows:

* SSH into `gehrels.uberspace.de` (username `kanthaus`). This can be done by password or SSH key.
* Navigate to the email folder (`cd users/firstname` where `firstname` is the email user name)
* Create a symlink to the `hello` account with `ln -s ../hello/ .hello`
* Add the `hello` folder to the subscriptions: `echo hello >> subscriptions`

There are some other folders inside our mailbox that you could be interested in mirroring in the own account:
* the "Mailinglists" folder, which receives messages from mailing lists of befriended projects and other initiatives in the area.
* the "Spam" folder, in which spam gets sorted automatically (and therefore not always accurately).

To subscribe to those as well, you can do again `ln -s ../hello/.Spam/ .hello_spam` and `echo hello_spam >> subscriptions` for the Spam folder for instance.

Once that is done the person should have access to Kanthaus email in their email client.

## Email filtering

The Spam filtering we use is [the default one offered by Uberspace](https://manual.uberspace.de/mail-spam/).

On top of that, we have extra rules to sort messages into folders, for instance for mailing lists. Those filtering rules are managed by [the Sieve system offered by Uberspace](https://manual.uberspace.de/mail-filters/).
If you want to change those filters, here are some possible steps:

* First, get hold of the password for our hello@ email account. It is in our password store.
* Then, install [a ManageSieve client](http://sieve.info/clients) and use it to connect to the hello@ address. You can pick from a wide range of clients. For instance, the `sieve-connect` command line interface is available on Ubuntu/Debian and works. The credentials required are:
  * Server: gehrels.uberspace.de
  * Port: 4190 (standard)
  * Username: The email address
  * The password for it
* Now you have access to the Sieve scripts available on the address and let you edit them.

