# :star: Kanthaus server

We have a server running locally that provides a few services to residents as well as guests.

## File sharing services

The server provides the possibility to store and exchange data.
Some services are publically available (e.g. connecting with an anonymous user), for others you need a user account with some privileges.
All file services are only available in the `full` [network](../technical/wifi.md) and served via [Samba](https://en.wikipedia.org/wiki/Samba_(software\)).
Use your computers file manager to browse the available network computers and locate the server as `KANTHAUS-SERVER`.
If this doesn't show up in your file manager or the link is broken, you can try entering `smb://kanthaus-server/` directly into your file managers adress bar. This should work on most linux environments.

### Getting a user account

To get a user account, speak to an admin. The admin will add you to the ansible user configuration and ask you to set a temporary password using your account. You can change the password yourself, e.g. via `smbpasswd -r kanthaus-server -U yourusername`.

Actually, you have two passwords:
* System user account: Used for local access and SSH access. Change password using `passwd` when logged in
* Samba account: Used for accessing the samba network shares. Change password using command above remotely or using `smbpasswd` when logged in.

#### Kanthaus cloud copy

* The share `kanthaus-public` offers an anonymously usable read only copy of the public part of the kanthaus cloud.
* The share `cloud.kanthaus.online` offers a read only copy of the whole kanthaus cloud. You need to have a user with the permissions class `kanthaus`.

The cloud copy is synchronized from the kanthaus cloud once every minute.

#### Internal cloud

The share `internalcloud` stores some data which should only be available from inside Kanthaus (e.g. financial data) and is only available to users with the permissions class `internal`. Please make sure to only put security sensitive stuff in here and also make sure to not leak your user credentials or the contents of this folder, when you have access to it.

This folder is part of the daily backup.

#### Home folder

Every user account also has their personal `home` folder available as the `homes` storage.
All data you put here is only available to yourself.
Inside the home folder, there is a directory called `storage`. This folder lies on an easily expandable, cheap harddisk storage. It is slower to access but suitable to store lots of data (e.g. backups of your computer).

Your home folder is part of a daily backup. Please put files called `.nobackup` into folders that you don't want to be backed up (e.g. to save storage space). The `storage` folder is **not** part of the backup, but the harddisks have a raid configuration to tolerate the loss of one harddisk.

## Shell access

When you have a user account, you can also use SSH to connect to the server and use it for computing tasks.
To set an initial password, ask an admin. When you already have file sharing access, you can add your SSH public key to the `homes/.ssh/authorized_keys` and use that for logging in.
Same as above: In your home directory there is a symlinked folder called `storage` which is on spinning disks, whereby the rest of your home folder is on limited SSD space.

## SFTP Access
You can use software like FileZilla to access your home folder through `sftp://kanthaus-server` providing your username. See the Shell Access section above for other details.

#### How to unlock the encrypted `kanthaus-server` via network
- be in `kh-admin`
- `ssh -p 2222 root@192.168.178.249`
    * your key must be stored on the server in `/etc/dropbear-initramfs/authorized_keys` -> `update-initramfs -u`
    * ED25519 key fingerprint: `SHA256:mvCVYx8D/Fv/qYq+a/H4MoRAcfExAUsAFW3L2NVHnD0`
- enter password (stored in keepass -> Server)

## System specs
System is designed to save power but still have some computing resources.
* CPU: Intel Core i5-4670 (4x 3.4 GHz)
* Ram: 8 GB DDR3L (If there is the need we have 16 GB available, just needs more power)
* SSD: 1 TB Samsung 860 Evo as root file system
* HDD: BTRFS pool with 3 disks, 1 redundant. Current usable size ~3.5 TB

## Backups
Backups are done using borgmatic. The backup target is the local harddisk storage, so it does not safe us against fire or theft of the computer. We might think about adding a remote backup as well.

## Other services
* Foodsharing Gitlab CI runner (dockerized)
* House bus services
  * local Web interface (dockerized)
  * Logging daemon to externally hosted influxdb
* Virtual machines (kvm/libvirt)
