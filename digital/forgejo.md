# Forgejo

We are in the process of migrating away from GitLab to a self-hosted Forgejo instance. This is a work in progress and this page documents the setup. For now, please continue to use [GitLab](gitlab.md) where our repositories are still hosted.

## Instructions for users

Go to https://git.kanthaus.online and log in with your Nextcloud account.
You will be automatically added to the "Kanthaus" organization in which our repositories are stored:
https://git.kanthaus.online/kanthaus

You can modify the files as you are used to from GitLab.

### Cloning repositories on your machine

If you are working with local clones, you need to update those to point to Kanthaus Git instead of GitLab.

On the page of each repository, such as https://git.kanthaus.online/kanthaus/handbook, copy the clone URL you prefer.
Then, in your local clone, run:
```
git remote set-url origin <new-url>
```

Note that if you want to use SSH URLs, you first need to [add an SSH key](https://git.kanthaus.online/user/settings/keys).

## Instructions for administrators

We are running Forgejo via Docker inside our Docker-Compose deployment of services on the `kanthaus-server`.

### Setting it up from scratch

After launching the service via Docker, the following manual steps need to be taken to configure the instance:
* Go to https://git.kanthaus.online/ where the set-up screen should display. Change the following settings:
    * Site title: "Kanthaus Git"
    * SMTP host: gehrels.uberspace.de
    * SMTP port: 587
    * Send email as: git@kanthaus.online
    * SMTP user: git@kanthaus.online
    * SMTP password: found in the Kanthaus keepass
    * Tick "Enable email notifications"
    * Tick "Disable Gravatar"
    * Tick "Allow registration only via external services"
    * Tick "Hide email addresses by default"
    * Domain for hidden email addresses: `git.kanthaus.online`
    * Set up an admin user with credentials you own: we will delete it later on
* After validating the sign-up form, go to https://git.kanthaus.online/org/create and create a "kanthaus" organization. You can prettify it a little, by adding a bit of metadata at https://git.kanthaus.online/org/kanthaus/settings and uploading a logo from https://kanthaus.online/pics/newLogo.png
* In Forgejo's configuration file (`/data/services/forgejo/config/app.ini`), add the following lines at the end:
```
[oauth2_client]
USERNAME = userid
ENABLE_AUTO_REGISTRATION = true
```
You can also take this opportunity to add `LANDING_PAGE = /kanthaus` in the `[server]` section.
And disable OpenID login:
```
[openid]
ENABLE_OPENID_SIGNIN = false
ENABLE_OPENID_SIGNUP = false
```
* Make sure our Nextcloud instance has the [OIDC Identity Provider](https://apps.nextcloud.com/apps/oidc) app installed
* Go to [Nextcloud's security settings](https://cloud.kanthaus.online/settings/admin/security) (while logged in with an administrator account in NextCloud) and generate a new OpenID Connect client (if it does not exist yet), with the following details
    * Name: Forgejo
    * Redirection URI: https://git.kanthaus.online/user/oauth2/NextCloud/callback
* Go to [Forgejo's Authentication Sources](https://git.kanthaus.online/admin/auths) and click "Add an authentication source", with the following settings:
    * Authentication type: OAuth2
    * Authentication name: "NextCloud"
    * OAuth2 provider: OpenID Connect
    * Client ID: the client ID produced at the previous step
    * Client secret: the client secret produced at the previous step
    * OpenID Connect Auto Discovery URL: https://cloud.kanthaus.online/.well-known/openid-configuration
    * Additional scopes: `email`
    * Claim name providing group names for this source: `roles`
    * Group claim value for administrator users: `admin`
    * Map claimed groups to Organization teams: `{"Kanthaus": {"kanthaus": ["Owners"]}}`
* Restart forgejo with `docker-compose restart forgejo` (in `/opt/kh-services`)

Note: this configuration relies on our NextCloud instance having the ["clean URLs" option](https://docs.nextcloud.com/server/27/admin_manual/configuration_server/config_sample_php_parameters.html) enabled. Otherwise, the URLs need to be adapted to add
`index.php` in them, such as `https://cloud.kanthaus.online/index.php/.well-known/openid-configuration`.

### Setting up SSH access

To let users clone via SSH, we need to take some additional steps. In the configuration file (`/data/services/forgejo/config/app.ini`), set those settings (if not set already):
```
[server]
START_SSH_SERVER = true
SSH_PORT = 2222
SSH_LISTEN_PORT = 2222
SSH_LISTEN_HOST = 0.0.0.0
SSH_DOMAIN = ssh.git.kanthaus.online
```

Then, set up a port forwarding in OpenWISP. You need to be connected to the `kanthaus-admin` network for this and go to
[the NAT Port Forward page](https://192.168.178.1/firewall_nat.php). Create a new rule:
* Interface: WAN
* TCP/IP version: IPv4+IPv6
* Protocol: TCP
* Destination: WAN address
* Destination port range: (other) 2222 to (other) 2222
* Redirect target port: 2222
* Description: git SSH access for Forgejo

The `ssh.git.kanthaus.online` should point to the Fritzbox (not kanthaus-server) so that it can take care of forwarding the port. If the domain points to kanthaus-server, connecting over IPv6 will likely fail.

### Setting up the continuous integration

In the configuration file (`/data/services/forgejo/config/app.ini`), set those settings (if not set already):
```
[actions]
ENABLED = true
```

Then, go to the [Runners Management](https://git.kanthaus.online/user/settings/actions/runners) page and click "Create new Runner". Copy the registration token and put it in `/opt/kh-services/.env` as `FORGEJO_RUNNER_REGISTRATION_TOKEN`.

Delete `/data/services/forgejo_runner/.runner` if it is present.

Restart the runner with `docker-compose up -d` in `/opt/kh-services`.

The runner will register and should become visible [in Forgejo's settings](https://git.kanthaus.online/user/settings/actions/runners).

For each repository where you want to use the runner, you need to enable actions in the repository's settings.

### Backing up Forgejo's data

You need to back up two things:
* the contents of `/data/services/forgejo`
* the Postgresql database. To do so:
    * First, find out the container id of the postgres instance with `docker ps | grep kh-services_postgres` and copy the first hash in the output to the second command, in place of `<container-id>`
    * Run `docker exec -it <container-id> pg_dump -U postgres --clean forgejo | gzip > forgejo_database_dump.sql.gz`

To restore the backup, you need to:
* restore the contents of `/data/services/forgejo`
* restore the database similarly with: `cat forgejo_database_dump.sql.gz | gunzip | docker exec -i <container-id> psql -U postgres forgejo`


