# :cloud: Nextcloud

We have our own instance of Nextcloud hosted on our server to store immutable data and our calendar.

### Who can reach it where?
The address is: https://cloud.kanthaus.online

You need to have an account to access the Nextcloud. So far only ten people have one, the plan is to link access to the position, so that every member and volunteer gets an account.

### How to mount it as a shared directory on my machine?

Nextcloud is reachable via the WebDAV protocol. On many Linux variants, you can connect to it in your file manager via the following address (where `${my_username}` is replaced by your Nextcloud username):
* `davs://cloud.kanthaus.online/remote.php/dav/files/${my_username}/` for Gnome
* `webdav://cloud.kanthaus.online/remote.php/dav/files/${my_username}/` for KDE
* You can also mount it manually as a filesystem with [davfs2](https://docs.nextcloud.com/server/13.0.0/user_manual/files/access_webdav.html#creating-webdav-mounts-on-the-linux-command-line)

More information about this setup is available in the [Nextcloud manual](https://docs.nextcloud.com/server/13.0.0/user_manual/files/access_webdav.html#accessing-files-using-linux), including [instructions for MacOS](https://docs.nextcloud.com/server/13.0.0/user_manual/files/access_webdav.html#accessing-files-using-macos) and [Windows](https://docs.nextcloud.com/server/13.0.0/user_manual/files/access_webdav.html#accessing-files-using-microsoft-windows).
