# :house: Expfloorer

The interactive digital map of Kanthaus, made in SVG with Inkscape.

## Access

- to the [map](https://kanthaus.gitlab.io/expfloorer/)
- to the [code](https://gitlab.com/kanthaus/expfloorer)

## Features

The different floors are collapsible. Toggles for beds, toilets, food, measurements, doors and windows.
More to be added in the future. More info in the repository.

## Extract and use svg

The example case of making a sleeping spot reservation sheet.

### Take svg from expfloorer

Every browser has a so-called 'developer console' that let's you get more information from websites. Let's use this feature to get a room shape. Go to [expfloorer](https://kanthaus.gitlab.io/expfloorer/) in your browser and press F12 to open the developer console. Now choose the inspector and inspect the element you want to extract.

![developer console in firefox](/images/expf2resSheet_1.jpg)
_In this example we extract the shape of the sleep kitchen._

All complex svg objects have a so-called 'd' value, which contains all the information about the shape. When you click that value in the developer console you can copy it.

### Insert svg into new file

Inkscape is the default svg manipulation program for linux, so we use it. To have a place to paste our d value we need to make a complex object and then change it. So we just make two overlapping circles or rectangles, select them and go for path->union. Now the two simple objects merged into one complex object. Now we open the XML editor, select the d value of our object and exchange the contents to what we copied from expfloorer.

![d value in inkscape xml editor](/images/expf2resSheet_2.jpg)
_The d value in the XML editor is clickable._

It might look like you just have an empty sheet after copying in the new d, but when you zoom out enough you will find your new object. It just doesn't appear in your previous viewport necessarily.

### Creating something

Now you can take the shape and do whatever you want with it. E.g. creating a sleeping spot reservation sheet like this one:

![finished reservation sheet](/images/expf2resSheet_3.jpg)
_Pretty isn't it? ;)_

But since this isn't an Inkscape tutorial we won't get into how to do that now.

