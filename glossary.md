# :page_facing_up: Glossary

<img src="/images/underConstruction.jpg" alt="under construction yeah" style="width:150px;"/>

Kanthaus has many terms that might be confusing. This page is meant to help restore clarity.

To make adding terms easier there is no order whatsoever. The best way to find what you're looking for is probably CTRL+f (which lets you search for terms on the whole page through your browser).



### CoMe
The weekly [Coordination Meeting](social/come.md)

### PlaMe
The monthly [Planning Meeting](social/plame.md)

### Power Hour
The weekly common cleaning. More info on [its own page](social/powerhour.md).

### Popcorn
In a round people speak (or pass on) one after the other, whereas a popcorn scenario means that people just speak up if they think of something. More info on [meeting culture](social/meetingculture.md)

### KMW
The van we owned until 2022. Abbreviation for its name 'KeinMüllWagen' (no trash wagon)

### HKW
The association that owns the house. Abbreviation for 'Haus Kante Wurzen w.V.'

### WaWü
The charitable association that runs projects in the house and beyond. Abbreviation for 'Wandel würzen e.V.'

### foodsharing
Big organization, community, [online platform](https://foodsharing.network) and method to save and share food. Starting point of the history that led to Kanthaus.

### yunity
Project emerging from foodsharing to make a new online platform to save and share everything. Context in which the people who would later found Kanthaus met. More info on [history](history.md)

### Karrot
[Online platform](https://karrot.world) for grassroots organizing of real-life activity. Over years co-developed by people in the house.

### Sociocracy
A governance system that uses small groups of people to make decisions based on consent and feedback loops. More info at [Sociocracy for all](https://www.sociocracyforall.org/sociocracy/). Kanthaus is _not_ organized sociocratically, but this, like other systems, was useful inspiration at many points.

### Score voting
A voting system where every options is rated individually on a scale and the option with the highest voting wins.

### Harzgerode/Freie Feldlage/FFL
Befriended project in the Harz mountains. More info on [their website](https://freiefeldlage.de).

### K20/Salzderhelden
[K20](https://k20-projekthaus.de/en/) is one of the projects closest to Kanthaus conceptually. [Salzderhelden](https://utopisches-salzderhelden.de/en/) is the village around, which houses a multitude of other, related project spaces.

### Exchange logic
The logic of reciprocity, which expects something in return for everything. A valued utopian thought in Kanthaus (and elsewhere) is that overcoming exchange logic is necessary for authentic human connection. Friederike Habermann is a German scholar who is passionate about the topic. [Here's a nice little essay of hers](https://wealthofthecommons.org/essay/we-are-not-born-egoists).

### K20, K22
Abbreviations for Kantstraße 20 and Kantstraße 22, the two houses that make up Kanthaus.

### Funkenhaus/Greene
[Funkenhaus](https://funken.haus/) is a self-organized seminar space. Greene is the surrounding village.

### Luftschlosserei
[Luftschlosserei](https://luftschlosserei.org) is a nice commune south-west of Leipzig.

### Danni
Short for 'Dannenröder Wald', which housed a [big forest occupation](https://waldstattasphalt.blackblogs.org/) in (mainly) 2020.

### Spinnerei/Lausitz
[Eine Spinnerei](https://www.eine-spinnerei.de/) is a project in the Lausitz region that focuses on lignate extraction protest, consequently sustainable renovation and free ways of education.

### Evaluation
Meeting about a single person and their relationship to Kanthaus as a whole. More info on [positions and evaluations](social/positionsandevaluations.md).

### Visitor/Volunteer/Member
The three positions a resident in Kanthaus can have. Residents can request a higher position at an evaluation. More info on [positions and evaluations](social/positionsandevaluations.md).

### VolMems
Short for 'Volunteers and Members'.

### ToI
Short for 'Time of Introspection', a time in which Kanthaus Volunteers and Members reflect on issues of the house. Usually once a year. There is no uniform format for this.

### MoC
Short for 'Month of Calm', a time in which no new Visitors are allowed to arrive so that residents don't need to worry about presenting Kanthaus and integrating new people. Happens irregularly.

### Alumni
Term for people who have been Volunteers or Members in the past. No official position yet, but it still is a thing.

### FFJ
Time of self-organized learning in a group. Happens in Kanthaus in 2024. More info on [the project's website](https://freiwilliges-freies-jahr.de/ffj-wurzen-2024/).

### BUND
[Bund für Umwelt und Naturschutz](https://www.bund.net) is a huge German environmentalist NGO. We run the local branch in Wurzen.

### Bufdi/BFD
Government-funded volunteer jobs.

### Pad
Online text editor for collaborative use. At [pad.kanthaus.online](https://pad.kanthaus.online) we run an instance of [HedgeDoc](https://hedgedoc.org/). More info on pad usage in Kanthaus e.g. in [CoMe](social/come.md).

### Script (CoMe context)
The residence script that runs in preparation of Come calculates who is due for an evaluation and how many resources were used in the last week, and posts the outcomes in a dedicated Slack channel.

### Residence record
Manually updated data base for the CoMe script.

### Communalizing
The act of declaring an item communal property.

### Privatizing
The act of declaring an item private property.

### Vortexing
The act of placing an item in the vortex. More info on the vortex in [stuff processes](social/stuffprocesses.md).

### Open Tuesday
Opening time of the ground floor of K22 which contains the free shop, the foodsharing Fairteiler and (sometimes) the repair café / maker space. Default time is 3pm to 5pm.

### free shop/Verschenkeladen
Room full of things to give away. Located in K22-0-3.

### Fairteiler
foodsharing term for a publicly accessible shelf or fridge in which food can be shared between strangers.

### Workaway
[Website](https://workaway.info) for people to find volunteer work all over the world. Kanthaus has a host profile on there.

### Projektverteiler
A mailing list for house projects and other alternative living concepts in Leipzig.

### HWR
[HWR](https://www.hwr-leipzig.org/) is short for 'Haus- und Wagenrat e.V.'. An association that offers guidance to house projects and trailer sites. They helped us aquire the houses in Kantstraße.

### NDK/D5
[NDK](https://www.ndk-wurzen.de/) is short for 'Netzwerk für demokratische Kultur', an association that has been doing work to counter the right-wing status quo in Wurzen for two decades now. [D5](https://www.ndk-wurzen.de/unser-haus/kultur-und-buerger_innenzentrum-d5/) are their headquarters at Domplatz 5.

### LADEN/Schweizerhaus Püchau
[The LADEN](https://schweizerhaus-puechau.jimdo.com/der-laden-wurzen/) is an open art space run by the association [Schweizerhaus Püchau](https://schweizerhaus-puechau.jimdo.com/) that has been doing art empowerment workshops in the region since 2010.

### Villa Klug
Something between house project and flat share with tons of space. Slightly south of Wurzen, right on the bank of the Mulde river.

### RE/S-Bahn
The two kinds of trains that go to Leipzig. RE is the fast one (~20 minutes) and S-Bahn the slow one (~40 minutes). More info on [travel](practicalities/travel.md).

### Phoenix
The red ebike. Usually lives in the communal bike shed.

### Lightning
The silver ebike. Usually attached to the kids trailer in the private bike shed.

### Daria
Bike trailer custom made for the base length of two green boxes. Usually stored on the wall of the communal bike shed.

### Nathanaël
Bike trailer custom made for the base length of four green boxes. Can also be used as a ladder. Usually stored standing outside of the communal bike shed under the roof.

### Carla (Cargo)
Heavy duty bike trailer. Inspired by [Carla Cargo](https://www.carlacargo.de/), built following the instructions provided by [Pedalkreis](https://pedalkreis.org/bauplaene/carlacargo/).

### Kids trailer
A boringly named bike trailer to transport two small children.

### Lollipop hitch
The kind of hitch to attach a trailer to a bike that is commonly used in Kanthaus. Like [this one](https://www.cykelportalen.dk/wp-content/uploads/2017/08/CarryFreedomCity02.jpg).

### MCM
Short for 'Mega/meta/master Coordination Meeting'. Format for longterm organizing that is not used anymore.

### Roadmap meetings
First longer term organizing format we came up with. Not used anymore.

### Sommerfest
Annual garden party in the warm season. With house tours, open free shop and Fairteiler, KüfA, drinks, music and more.

### KüfA
Short for 'Küche für Alle' or 'kitchen for all', an event where food is handed out for free or donation-based.

---

- doocracy
- dragon dreaming
- scrum
- jitsi
- bigbluebutton
- nextcloud
- nuudel
- inventaire
- open refine
- blindspots
- Jugendkommune
- ROAW
- collective governance directory
- MSV
- Kita
- Bubis/Bubi/Mubi
- communal
- sharing circle
- Fokusgruppe (Nachhaltigkeit)/FGNW
- Wenceslaigassenfest
- potato action
- Auerworld
- Acroyoga
- GFK/NVC
- Kommuja
- GEN
- Eurotopia
- Awareness
- Privilege
- FLINTA*
- FTT
- Kimchi
- Tempeh
- Landgut Nemt
- Spidey
- (ebay) Kleinanzeigen
- (occupancy) indicator
- Window holder
- Sound bowl
- Guest book
- Expfloorer
- House bus
- Ventilation system
- Heatpump
- AC/air conditioning
- Bike repair station
- 