# :paperclip: Core documents of Kanthaus governance

We - that is mostly Doug - spend a lot of thought on how to organize ourselves. That led to a three layer system of governance, which is described in the following documents.

### [The Constitution](https://kanthaus.online/governance/constitution)
- The most important document that describes the basic structures of communal living and decision making.
- It is written in formal language on purpose to make it a legit and clear legal document.
- The more human-friendly version that contains explanations in casual language is [the FAQ](https://kanthaus.online/governance/governancefaq)

### [The Collective Agreements](https://kanthaus.online/de/governance/collectiveagreements)
- The document that clarifies more practical questions of everyday life.
- The schedule of common activities and the preferences regarding certain practicalities are written down here.
