# :package: Processes to manage stuff

<img src="/images/underConstruction.jpg" alt="under construction yeah" style="width:150px;"/>


'Stuff' is the term widely used in Kanthaus to refer to physical items that take up space in the house and are not food. Stuff can be useful or not, communal or private, a blessing or a curse. Managing stuff is a vital administrative task of the house, so we came up with several systems.

## The vortex

Located in the [Intermediate Storage Room](/rooms/intermediate.md) in K20-1-5, the vortex is a physical space to regularly look through potentially unwanted communal items.

### What is it for?

There are three major use cases for the vortex:
1. **Removing** stuff from the household.
1. **Clearing** a corner of the household.
1. **Adding** stuff to the household.


1. Is probably the most relevant case: Someone thinks a certain item should not be part of the Kanthaus household anymore and wants to start the process of getting rid of it. The vortex gives others time to take it back and/or privatize it.
2. When someone tidies up a cluttered space they usually find stuff that is clearly (or maybe) not trash but still does not belong there. Needing to find a good new or the proper previous location os hard and blocks tidying up efforts, so putting these things in the vortex is an appropriate way to go.
3. Sometimes people find stuff that might be useful to the house or individuals, but they're not really sure. Putting it in the vortex means leaving the decision to communalize or privatize the items to others and just giving them prioritized access before it ends up in the [Free Shop](/rooms/freeshop.md).

### How does it work?

There are three phases which the stuff - if not taken out - travels through. 'Vortex shifting' is a [Power Hour](/social/powerhour.md) task, which means that it ideally happens every week, sometimes more seldom, never more often.

#### Phase 1 (0-7 days)

- All new items go here.
- If you have a certain reason for sorting out something (e.g. broken, we have too much of this, etc.), please add a note so others can understand your thoughts.
- The shift should always happen in [Power Hour](/social/powerhour.md) before other things are added to phase 1.
    - This means depending on when an item is added to the vortex, it might only stay in phase 1 for a very short time before it is shifted to the next phase.

**Which items can be saved back from the vortex at this stage?**
<br> Only your own personal items.


#### Phase 2 (7 days)

- Items from phase 1 are shifted here during [Power Hour](/social/powerhour.md).
      
**Which items can be saved back from the vortex at this stage?**
<br> Your own personal items.
<br> Items you know to be communal.

#### Phase 3 (7 days)

- Items from phase 2 are shifted here during [Power Hour](/social/powerhour.md).

**Which items can be saved back from the vortex at this stage?**
<br> Your own personal items.
<br> Items you know to be communal.
<br> Items that might have been private before can now be communalized.
<br> Items that might have been communal before can now be privatized.

#### Doom

- Items from phase 3 are doomed during Power Hour, this means:
    - items that make sense to communalize are sorted into fitting communal locations
    - trash is put into bins accordingly
    - good but unneeded items go in the [Free Shop Storage](/rooms/freeshopstorage.md)

          
**Which items can be saved back from the vortex at this stage?**
<br> Privatize whatever you want.
<br> Only (re-)communalize stuff if you're really sure about it and know a good location for it!

#### Details of the shifting

To make it possible for Kanthausians who are not currently in the space to also voice an opinion on the stuff in the vortex, it's required to document vortex shifts on [Slack](/digital/slack.md).

Experience leads to the following best-practice of vortex shifting:
- Start on the back and empty phase 3.
    - Make (a) box(es) with things for the [Free Shop](/rooms/freeshop.md)
    - Don't be shy to use the trash bins!
- Move phase 2 into phase 3.
    - Take pictures and make sure that the things are identifiable.
    - Feel free to also use the space below the table.
    - If more space is needed than before, you can move the tape that marks the border between phase 2 and phase 3.
- Move phase 1 into phase 2.
    - Take pictures if you're motivated, but the 'phase 2 to phase 3' pics are more important.
- Add stuff that came in during [Power Hour](/social/powerhour.md) to phase 1.
- Upload the pictures to #kanthaus-stuff on [Slack](/digital/slack.md) and add some descriptive words.

## Decluttering actions

Formerly known as 'sales' (based on the idea of doing an 'internal yard sale'), which admittedly is an extremely confusing term, decluttering actions are events to clear out storage areas.

### Communal stuff decluttering

On example for communal stuff decluttering is the K20 entrance with its communal shoe and jacket shelf. Let's say we want to declutter the jacket shelf, the process is as follows:

1. Empty the shelf and put all its contents to an accessible location. People need to be able to look through everything there. It should also not be in the way. Past locations were the [Dragon Room](/rooms/dragonroom.md) or [Elephant Room](/rooms/elephantroom.md) tables.
1. Specify a time frame for the event and communicate it on site (the shelf itself, the sorting area and on [Slack](/digital/slack.md)). Past actions usually took 1-2 weeks maximum.
1. During the event people are expected to take out the things they want to keep and hang them back onto the shelf.
1. When the time is up everything that's left will go through the vortex.

### Private stuff decluttering

(tba)
- 'internal yard sales'
- LDA in spitz storage room

## Privatization of communal stuff

(tba)