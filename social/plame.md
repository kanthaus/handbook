# :clipboard: Planning Meeting

<img src="/images/underConstruction.jpg" alt="under construction yeah" style="width:150px;"/>

The monthly meeting to discuss bigger topics relevant to the house as a whole. Decisions can be made and participation of Volunteers and Members is expected if they are around.