# :white_check_mark: Coordination Meeting

The weekly meeting to coordinate everyone's actions and keep up-to-date with what's going on in/with the house. Colloquially called 'CoMe'.

### Time and place

The default is **Monday 10 am** in the [dining room](/rooms/diningroom.md). The meeting lasts until 11 am maximum.

### Scope and importance

This is the most important meeting at Kanthaus. It takes place every week, we take notes which we [publish on our website](https://kanthaus.online/governance/minutes) and we expect people to take part if they want to know what's going on.

### Progression

A facilitator guides through the meeting, reads out the relevant informational sections and guides through the discussion points. Topics are collected before-hand in the [CoMe pad](https://codi.kanthaus.online/come). This document is then used as base for the meeting by the facilitator.

### Adaptability

The CoMe pad is not subject to the [core documents](documents.md) which means that facilitators can change it as they see fit and add or omit sections to their liking.

#### Sections which have been used reliably for a long time are (in chronological order):

##### Introduction & review of the week before
- a greeting
- a moment of silence
    - to focus the attention
- a brief [check-in round](https://medium.com/range/check-in-rounds-a7737012fed5)
    - to have hear everyone's voice and be able to understand their current mode
- finding volunteers for the imminent meeting tasks
    - meaning someone to take notes, someone to update the physical board, and someone for the digital calendar 
    - the volunteer to clear all [reservation sheets](/practicalities/roomreservation.md) after the meeting is also found now
- an overview of resources used in the last week (e.g. water and electricity)
    - to give people a feeling of how much we as a group consume
    - the format has been changed many times and is still not satisfying
- irregular communal income and expenses
    - for transparency
- a summary of things that happened in and around Kanthaus
    - to celebrate achievements

##### Planning of the current week
- an overview of planned arrivals and departures this week
    - to nurture and/or protect the community feeling
- the weather forcast
    - to take into account for the schedule
- who is due to be [evaluated](positionsandevaluations.md) or have a visitor check-in
    - to be able to schedule them
- **the schedule for the week**
    - 1st main part of the meeting
    - creates the communal week plan
- if someone wants to get/buy something
    - to find out if buying is necessary and/or get approval from the group
- **discussion and announcements**
    - 2nd main part of the meeting
    - the space for people to raise points concerning the current week and/or near future
- [task lottery](https://kanthaus.gitlab.io/task-lottery/) and food planning
    - to distribute communal dinner preparation and hosting of the free shop opening

#### Sections which are new, have been there in the past or are used only sometimes are:
- an selection of subjectively interesting world news
- a tiny version of [Tekmíl](https://theanarchistlibrary.org/library/tekosina-anarsist-tekmil-a-tool-for-collective-reflection) or a space for potentially critical feedback to others present
- a to-do list for fixes and improvements around the house
- probably more that got forgotten over time...
