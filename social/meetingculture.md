# :busts_in_silhouette: General meeting culture

<img src="/images/underConstruction.jpg" alt="under construction yeah" style="width:150px;"/>

Kanthaus wants to be efficient and respectful of people's time. We acknowledge the fact that in-person meetings are not the best format for all kinds of work and try to be accessible also to people who are physically distant.

## Meeting frames

- We specify clear start and end times.
- We start on time and don't wait for late people.
- We watch the time and don't drag out the meeting.
- We have a facilitator who guides through the meeting.

## Hand signals

To make it possible to give silent feedback we use [hand signals](https://www.collaborationsuperpowers.com/wp-content/uploads/2020/04/Handout-Hand-signals-for-remote-meeting-facilitation.pdf).

![hand signals](../images/handSignals.jpg)
_More can be seen in the linked pdf._

Their use is to 'comment' without disrupting the speaker, to facilitate the order of vocal contributions as well as pointing out technical or language points.

Additionally to the list above we sometimes use the so-called 'temperature check' to get an understanding of the feeling of the group towards a certain issue. To achieve that one person asks the group for said temperature check and then everybody wiggles their hands either downward (cold, no support, maybe even resistance), upward (hot, big support, enthusiasm) or somewhere in the middle.

## Pads

Our main meetings (like [CoMe](come.md) or [PlaMe](plame.md)) usually are backed up by corresponding online pads which contain the procedure and topics visible and editable for everyone. This means that people often bring laptops or phones to follow what's going on. Facilitators in turn usually don't readout everything or explain the format in the beginning, since it's technically available already.