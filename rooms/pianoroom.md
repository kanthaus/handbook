# :musical_keyboard: Piano Room (or K22-1-4)

The piano room is a big and cozy community space with bookshelves, a projector, a good sound system and a range of musical instruments which also includes a piano.

### Facilities
- Many comfy sitting spots on two couches, an armchair and two beanbags
- Many blankets and pillows
- A projector that is mounted to the ceiling
- The two main bookshelves which are sorted by topic
- A piano, three guitars, two violins, a cajon, two drums, a harmonica and many notes and songbooks
- A computer that is connected to the Kanthaus network

#### Projector
The native plug to connect to the projector is DVI, but we attached a cable with HDMI adapter.

There are two more adapters permanently stored next to the projector. Feel free to use them but don't take them away!
- mini display port
- display port

The projector's resolution is 1280x768, you might need to adjust this manually for the picture not to be cropped on the sides.

#### Bookshelves
The books in the piano room are for everybody to read, so feel free to use them! Please make sure the order doesn't get disrupted though. If you want to have a glimpse of some of the things you can find there, have a look on [inventaire](https://inventaire.io/groups/kanthaus)! (The list on there is far from being comprehensive though.)

### Room availability
This is a public room, so it is always accessible to people. Sleeping in here is probably a bad idea and might irritate some people. Napping however is fine as long as you don't expect people to be super silent just because you chose a public room to relax in.

### Specials
Since this is one of the most public rooms, please make sure to keep the space free of personal belongings if they are not in immediate use!
