# :computer: Silent Office (or K20-0-2)

The silent office is a space for silent office work on your own projects.
It provides desk space for 5 people, including a few monitors, keyboards and mice.

### Facilities

* Privatizable desk spaces
* Office equipment like monitors, keyboards, mice and cables
* Water tab for filling cups
* Some small storage space and office supplies.

### Room availability

The silent office is a public room that should be available to people wanting to not be disturbed when working.

The room can be booked for certain events (e.g. Hackathons) that might relieve the non-disturbance rule to their own needs.
Otherwise, it should be available for anybody to work in here.

### How to handle private things/desk clutter

If you reside in Kanthaus mainly for working on a specific thing, you might want some working material always available.
It might come convenient to leave your paperwork/usb stick/cup on the desk while you go for food, for a walk or to sleep.
This is okay, as long as you plan on using the same desk again soon and use it continuously for multiple hours in a block.

If you are only spontaneously looking for a space here, please take away all the items you brought with you when leaving the room.

There is limited storage space available; this should be available to the people who work in this room regularly and might need access to those.

### Specials

* The radiator in this room should be kept at 5 when warmth is wanted. The radiator in this room is relatively small so this room will unlikely get too hot as the heating system is limited/controlled globally. The fans underneath the radiator boost it's efficiency, and are controlled via wifi. If they are repeteadly running while the heating is off, then it might be worth reporting it. Turn the radiator nob to 0 when the room will be empty for more than some hours.
