# :womans_clothes: Communal Closet (or K20-2-4)

The communal closet is a room for shared and personal clothes and belongings.

### Purposes
- storage for clean communal clothes and bedclothes
- storage for private things

### Specials
- 'Clothes 4 future'-rail: Clothes that have been worn before but don't need to be washed immediately are waiting for another round here.
