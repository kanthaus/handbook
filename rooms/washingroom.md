# :cyclone: Washing Room / Dumpster Kitchen (or K20-0-1)

A room for sorting and cleaning clothes and food.

### Purposes
* wash and dry potentially dirty saved food
* wash laundry
* storage for green boxes, big plastic bags and bike bags for dumpster diving
* storage for categorized dirty laundry

### Specials
* waching machine can operate with rain water
* waterproof screen and sound system provide entertainment while washing food

---

### Laundry
We have a pretty elaborate idea of how communal laundry should be done. Please refer to the [laundry page](/practicalities/laundry.md) for further details.

### Food
You may find saved food in here which needs cleaning and sorting. Saving food is one of the main tasks in our house. [Here's the guide](/practicalities/savingfood.md) to do it properly.
