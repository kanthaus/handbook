# :bread: Snack Kitchen (or K20-1-1)

Here people can prepare snacks without being in the way of other people who actually cook.

### Facilities
- The fridge
- A sink
- The bread station which contains the toaster, the bread cutting machine, the mini and midi ovens
- The microwave
- The tea and coffee station with dry coffee and tea, thermos, electric kettle and space to put ready-made tea and coffee
- The dishwasher
