# :couple: Communal Sleeping Room (or K20-2-3)

The communal sleeping room is group sleeping room.

### Facilities

- Mattress space that spans the whole room length from one wall to another (1x small and 2x big mattresses)
- One individual mattress behind the door
- A sound system

### Specials

- Beds can be claimed for an unlimited amount of time in this room. Refer to the sheet on the door for the current allocation.

### Cinema

**Unfortunately the projector broke! The cinema setup is not usable for now.**
But the rest of the setup is still in place.

This room can and should be used for watching movies in a nice, cozy atmosphere.
Before watching a movie late in the evening, communicate to the others when you are about to start and how long it might take, so people can arrange other sleeping possibilities if they might be disturbed by it.

![Projector/sound setup](/images/communal-sleeping-kino.jpg)

The projector can be attached via HDMI or Mini-Displayport.
An audio cable is attached to the projector so that you only need a single connection from your device.

Volume can be adjusted with the top knob at the subwoofer next to the projector.

Please plug/unplug the system before/after you use it at the plug near the pillows.

If you want to watch movies during the day you can use the blinds which are installed on the windows to shut out most of the daylight.
