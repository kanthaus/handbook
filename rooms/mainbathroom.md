# :bathtub: Main Bathroom (or K20-2-1)

This room acts as a bathroom which should mainly be used for showering.

### Facilities
* Bathtub with warm shower
* In-use towel drying station
* Toilet (tap-water driven) with bad flushing behavior
* Handwashing basin (cold) with mirror
* Towel storage (in the small adjacent room)
* Dirty laundry baskets (in the small adjacent room)
* Blanket storage (in the small adjacent room)

### Door
Using the door of this room is quite loud, which is especially disturbing when people are already sleeping in the rooms around. A pragmatic solution is to always keep it open! :)  
If you want to close it for having privacy in the room, please try to do so silently (e.g. slow or by pushing the door slightly orthogonally to the moving direction).

### Hot water
The shower uses the solar energy generated on the roofs. If it's nice and sunny you'll probably have the heat of the water for free! :) Just check the temperature indicator on the boiler to see if the water is already hot or needs to be heated up first.

If solar power is not enough to generate the necessary heat, a [Clage MCX 7 6.5 kW electrical passthrough heater](https://www.clage.de/de/Produkte/E-Kleindurchlauferhitzer/MCX) is in effect. It automatically switches on when you turn on hot water in the shower to generate the wanted temperature. The heater has three settings which heat the water to different temperatures as indicated below:

* ECO - green light (35 degrees)
* COMFORT - orange light (38 degrees)
* MAX - red light (45 degrees)

To change the setting of the heater you just need to press the button in the middle. Your fingers need to be dry for it to work reliably.

The heater has limited power and sometimes is not able to heat up very cold water fast enough to provide the wanted heat as well as strong water pressure. The light on the device will blink if this is the case.

The default setting of the heater should be **COMFORT**, so please put it back to that after you used and changed it.
That is a comfortable shower temperature with high comfort as the temperature stays quite constant.

### Showering

When you shower, make sure that the shower curtain is hanging inside the shower, so the water does not run on the floor.

Also, please either dry yourself inside the shower or use the towel laying on the bathtub (or a new one if there is none) to put on the floor in front of the shower.

![Don't: Wet floor because somebody did not dry inside the shower AND did not use a foot towel](/images/bathroom-wet-floor.jpg)<br />
_Example of a wet floor. Walking on this with slightly dirty shoes/feet/socks will spread a lot of dirt everywhere!_

After you showered, the room needs to be ventilated to bring the moisture down. You can do that by opening the window for some minutes.

### Heating
This room is only ever used shortly, so the heat is mostly lost.
Please do not heat this room!

An exception is in the cold winter: If it is below zero for prolonged time periods, keep the radiator set to "\*" so the mold stays away. During the heating period it's also very important to keep the door shut.
