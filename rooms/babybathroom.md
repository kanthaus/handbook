# :baby: Baby Bathroom (or K22-1-2)

<img src="/images/underConstruction.jpg" alt="under construction yeah" style="width:150px;"/>

### Toilet flush

The original flush has the problem of not closing properly which lead to a lot of wasted water. That's why we removed the cover and operate it manually now. This is how it looks like:

![Baby bathroom toilet flush](/images/babybath_toiletflush.jpg)
_Don't be afraid to touch this... ;)_

The white part on the left (let's call it flushing part) needs to be lifted for water to flush down into the toilet. Letting it go down again will stop the water flow.

#### Troubleshooting

**If the water doesn't stop running down into the toilet**, there might be some dirt that prevents it from closing properly. Just move and turn the flushing part to 'clean' the orifice it's supposed to close.

**If the tank in the wall doesn't stop refilling**, the black part of the filling valve is probably stuck below the white frame. Look at the picture above to see that it needs to be *above* the white frame.