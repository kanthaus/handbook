# :package: Spitz Storage Room (or K20-4)

This room is dedicated to storage of items that do not need to be frequently accessed. 

## Personal stuff 

Up to 3 stacked banana boxes (or equivalent volume), per person. Non volunteers and members need to update the Last Date Accessed on their boxes at least once a year (by crossing over the previous date on the label of the box, and adding the current one). If their stuff is found to be over a year since the LDA, then the stuff can be vortexed, and the owner notified (if known and possible). Adding contact information could be a good idea.
Ideally nobody should need to move other people's stuff to access their own.


## Communal items  

Ideally we would circumscribe the storage to certain general categories of stuff, to easily assess if a wanted item could be found there or not. Stuff that is not readily obvious should be clearly labeled, with as much useful information as possible (i.e.: responsible/knowledgeable person/s, status or condition, amounts, instructions, etc).

## Notes 

The marked corridor should be free of stuff standing or poking into, this way it is easier and safer to move around carrying stuff.

Banana boxes can easily be sourced by going to supermarkets and asking employees for them.

There is a long purple ethernet cable under the ventilation pipes cover. 

There is no ventilation in this room.

Do not forget to turn off the light when leaving. The red witness light in the switch is on when the light is on.

