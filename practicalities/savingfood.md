# :pear: Saving food

Almost all the food we eat is saved. Here you can find out how that works.

### Getting the food

The first step is, of course, to get the food into the house. We have two main ways of doing so:

1. foodsharing pickups

Officially agreed-on cooperations with stores are the best way to save food. There are cooperations with different kinds of stores in the area. [foodsharing](https://foodsharing.network) is not exclusive to Kanthaus and signing up for a pickup requires being a verified foodsaver on aforementioned page. If you're interested but need help, talk to Janina.

2. Dumpster-diving

We also save food from dumpsters of supermarkets. We have several good spots we usually visit by ebike. If you want to see the true extent of food waste for yourself, you should really join in and take a look into a dumpster! Talk to anyone in the house to get involved.

When you come back from a successful food haul, you'll have crates and bags full of very ripe fruit, partly foul veggies and potentially soiled packaged food. Leave those crates and bags on the floor of the K20 entrance to be found by people in the morning.

### Sorting and cleaning the food

Everybody will be thankful to people who clean and sort food! Here's how you go about it:

(tbc)
