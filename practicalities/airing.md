# :wind_chime: Airing

We do have a [ventilation system](/technical/ventilation.md) in place that takes care of most of our ventilation needs. Usually you do _not_ need to open windows to achieve air exchange!

In the past we needed to _make_ people air, nowadays it's more about stopping them from doing it wrong.

### Unhelpful airing

Airing practices to be avoided are:
- leaving windows tilted, especially in cold temperatures (negligable airflow, massive loss of heat)
- opening windows without making sure the right bolts are open/closed (the window might actually fall out!)
- closing windows without closing the bolts (maybe the next person doesn't check? and heat loss)

Yes our windows are old und difficult to handle. Sorry about that.


### General information

The information below stems from the initial version of this article (before we hat the [ventilation system](/technical/ventilation.md)). It still holds true and remains here to illustrate why air exchange is useful in the first place:

Rooms need to be aired for some reasons:
* Decrease in air quality (when people reside in there)
* Getting smell/dust out (Working, painting, cooking)
* Bringing humidity to a lower level to avoid condensation/mold (mainly when the average outside temperatures are below the average inside temperatures)

Airing is most efficient when–
- windows are fully opened and not just partially.
- windows on both sides of the building are opened simultaneously, so that wind can go through.
- done more often but less long, especially when it's cold. This way the air in the room can be exchanged without losing too much heat.

More detailed information on this can be found in [this guide on how to handle doors and windows](doorwindowhandling.md).
