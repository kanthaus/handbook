# :wind_chime: Ventilation

Kanthaus has a [central heat-recovery ventilation system](https://en.wikipedia.org/wiki/Heat_recovery_ventilation), consisting of a [Systemair SAVE VTC 700](https://shop.systemair.com/de-DE/save--vtc--700--l--wrg--geraet/p609989) ventilation unit in the K22 attic and an extended system of pipes in valves to the majority of the rooms.

The ventilation unit supplies fresh air to the house, pre-warmed by the stale air it extracts from the house. Both air flows do not mix, they just transfer the heat inside the heat exchanger.

The system automatically adjusts the fan speed to maintain a consistent relative indoor humidity, thereby saving electric power.

## Air flow schema
![](/images/ventilation-plan.png)

There are exceptions between floors:
- Silent Office has supply instead of intake
- K22-0 just has one intake in the free shop storage, but no supply - it pulls fresh air from the hallway and through the freeshop lounge

Also, ventilation piping is work-in-progress and will be extended as the house evolves.

## Statistics

You can get some statistics about the ventilation system in [Grafana](../digital/grafana.html). Those statistics are pulled from the ventilation unit via [an ESP32 device communicating with it via Modbus](https://gitlab.com/kanthaus/ventilation-watcher).

## Maintenance

- Intake filters in the rooms: change and wash every 1-3 months when heating is running
- Central filters in the attic: replace and buy new filter every 12 months, ideally before winter
- Heat exchanger in the attic: wash every 1-2 years, maybe?

### Change and wash intake filters
They are located in all rooms where air is sucked into the pipe, to prevent the pipe from getting dirty.
Especially when radiators are running, a lot of dust is kicked up in the air and ends in the filters.

To check if a change is necessary, measure the air flow into the intake with an air flow meter (currently in K22-1-1).
It doesn't hurt to replace the filters more often.

You can take the filters out by simply pulling out the valve.

Fresh filters are in a box, currently in the Ex-food-storage (K22-1-1) as of 2023-02-23. Put the new filter on the outside of the pipe and insert the valve again. It's a bit tricky to prevent the filter from getting pushed too far into the pipe.

You can wash the filters with a lot of water and dry them. Then they can be reused a few times, until holes appear.

### Central filters in the attic
The SAVE VTC 700 ventilation devices has two filters, one for fresh air and one for used air. It's important to replace the fresh air filter every 12 months, because it gets filled with dust and dirt from outside air. Especially spring and summer add a lot of pollution. When the filter gets full, the air supply can get smelly and the ventilation system doesn't provide as much air. Hence, it's good to replace the fresh air filter in autumn.

The used air filter doesn't need to replaced regularly, as we have to filters on the intakes in the room (see section above).

First, buy a new fitting F7 filter, like those:
- https://www.as-luftfilter.de/Ersatzflter-Lueftungsgeraete/Systemair/VTC-700/Ersatzfilter-passend-fuer-Systemair-VTC-700---Gueteklasse-F7.html
- https://rt-handel.de/Ersatzfilter-Lueftung/Systemair/VTC-700/Ersatzfilter-passend-fuer-Systemair-VTC-700-Filterklasse-F7

Then turn off the ventilation device, open the main cover and unscrew the left panel. Slide out the old filter and take a picture to show it to interested people ;) Insert the new filter, carefully screw the panel on, close the main cover and turn on ventilation again.

### Wash heat exchanger
We didn't do this so far, but the heat exchanger can be rinsed with water to get rid of fine dust. This should increase its efficiency.


