# :space_invader: 3D printer manual

Location: Kanthaus electronics workshop in K22.

## Description


The printer model is Prusa i3, despite what the labels say.

The extruder nozzle is 0.5mm in size.

The currently used plastic is PLA.


## Usage

### Prepare your model for printing

1. Get a .stl file

2. Install slicing software (e.g. [Cura](https://ultimaker.com/software/ultimaker-cura/))

3. Load the .stl file into the slicing software

4. Slice! (Enter/verify settings for the printer when prompted.) You'll get a .gcode file.

### Prepare for printing

1. Plug in the power cable

2. Install monitoring software (e.g. Cura, pronterface)

3. Plug in USB cable

4. Start the monitoring software

5. If the printer is not detected, choose the serial port (/dev/ttyUSB*) and model and connect


#### Clean the bed

1. Make sure the bed is cold

2. Use your fingernails to scrape off remains of the previous print


#### Level the bed

Moving the bed using the built in controls is possible but time-consuming. Here's instructions for the laptop.

1. Click the "home" button

2. Raise the extruder by 2cm using the software controls.

3. Move the extruder near one of the corners closer to you (doesn't have to be very close).

4. Lower the extruder step by step without hitting the bed. The green LED will light when the extruder is close to the bed. Pay attention to that moment.

5. Lower the extruder until it touches the bed. If it doesn't, raise it until the LED goes dark, and then turn the screw under the corner on the bed so that the LED shines. Raise the bed by 1-2mm. Careful - if the bed is very uneven, the screw will turn without effect. Then just continue.

6. Raise the extruder to the point where the LED goes dark. Then lower it a little so it's lit. Press on the bed - the LED should go dark again.

7. Move the extruder sideways to the next corner, by small steps. When the LED goes dark, adjust the screw in the target corner. Set it so that pressing on the bed turns off the LED.

8. When the corner is reached, move to the next corner. Do about 2 laps.


### Print

1. Close all instances of printing or monitoring software

2. Start printing software (e.g. Cura, pronterface)

3. Load .gcode file

4. Start printing

The printing will begin by heating the extruder and the bed. Then the actual plastic will start coming out.